module.exports = {
  theme: {
    extend: {
      colors: {
        background: '#F8F8F8',
        primary1: '#002B60',
        primary2: '#EF5896',
        footer1: '#F7B5C1',
        footer2: '#FFEDC5',
        timeline: '#F8B6C1',
        label: '#F8BC36',
        button: '#EF5896',
        buttonDone: '#14C6B0',
        caption: '#747880',
        alertSuccess: '#C5DE92'
      },
      fontSize: {
        '12px': '12px',
        '14px': '14px',
        '16px': '16px',
        '20px': '20px',
        '24px': '24px',
      },
      width: {
        '250px': '250px',
        '100px': '100px',
        '50px': '50px',
      },
      height: {
        '250px': '250px',
        '100px': '100px',
        '50px': '50px',
      }
    },
  },
  plugins: [
    require('flowbite/plugin')
  ],
  content: [
    "./node_modules/flowbite.{js,ts}"
  ],
}